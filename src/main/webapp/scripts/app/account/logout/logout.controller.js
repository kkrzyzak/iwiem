'use strict';

angular.module('iwiemApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
