/* globals $ */
'use strict';

angular.module('iwiemApp')
    .directive('iwiemAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
