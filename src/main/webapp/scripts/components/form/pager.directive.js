/* globals $ */
'use strict';

angular.module('iwiemApp')
    .directive('iwiemAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
