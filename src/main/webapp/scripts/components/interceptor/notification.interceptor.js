 'use strict';

angular.module('iwiemApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-iwiemApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-iwiemApp-params')});
                }
                return response;
            },
        };
    });